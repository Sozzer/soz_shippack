package data.shipsystems.scripts.ai;

import java.util.List;

import com.fs.starfarer.api.combat.*;
import org.lazywizard.lazylib.MathUtils; //Or at least, I think this is necessary. I dunno, it's like 11pm and I'm tired.
import org.lwjgl.util.vector.Vector2f;

import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.util.IntervalUtil;

public class soz_fluxswapAI implements ShipSystemAIScript {

	private ShipAPI ship;
	private CombatEngineAPI engine;
	private ShipwideAIFlags flags;
	private ShipSystemAPI system; //Turns out I can't remember which of these I need.
	
	public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
		this.ship = ship;
		this.flags = flags;
		this.engine = engine;
		this.system = system;
	}
	
	private float bestFractionEver = 0f;
	private float sinceLast = 0f;

    private final IntervalUtil tracker = new IntervalUtil(0.21f, 1.24f);

	@SuppressWarnings("unchecked")
	public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
		tracker.advance(amount);
		
		sinceLast += amount;
		
		if (tracker.intervalElapsed()) {
			if ((ship.getHardFluxLevel()) >= 0.80f || ship.getFluxTracker().getFluxLevel() >= 0.80f) {
			ship.useSystem(); //RAAAA WHY WON'T IT FUCKING WORK
			}
		}
	}
}
